import glob
import sys
import errno
import re
fin=str(sys.argv[1])
fout= str(sys.argv[2])
files = glob.glob(fin)
fo=open(fout,'w',errors='ignore')
with open(fin,'r',errors='ignore') as fi:
 for line in fi:
  line=re.sub('[\(\)\{\}<>]', ' ', line)
  line= re.sub(r'[?|$|.|!|,|"|:|;|-|_|>|<]',r'',line)
  line=line.replace("-"," ")
  words=line.split()
  for w in words[1:]:
   fo.write(w)
   fo.write(" ")
  fo.write('\n')