import sys
import math
from collections import defaultdict
import itertools
import codecs
import re
finput1 = str(sys.argv[1])
finput2 = str(sys.argv[2])
finput3 = str(sys.argv[3])
finput4 = str(sys.argv[4])
f5 = open("results.txt","w")
search = '***NAMEDCLASSES***'
label = {}
j = 0
w = {}
label1 = {}
k = 0
w1 = {}
v = 0

with open(finput1,'r') as f1:
 for line in f1:
  if line.startswith(search):
   words = line.split()
   label[j] = words[1]
   w[j] = defaultdict(int)
   j = j+1
   line = next(f1)
  words = line.split()
  if len(words) == 2:
   w[j-1][words[0]] = int(words[1]) 
   
 
with open(finput2,'r') as f2:
 for line in f2:
  if line.startswith(search):
   words = line.split()
   label1[k] = words[1]
   w1[k] = defaultdict(int)
   k = k+1
   line = next(f2)
  words = line.split()
  if len(words) == 3:
   w1[k-1][words[0]+words[1]] = int(words[2]) 
   
with open(finput3,'r') as f3:
 for line in f3:
  if line.startswith(search):
   words = line.split()
   label1[k] = words[1]
   w1[k] = defaultdict(int)
   k = k+1
   line = next(f2)
  words = line.split()
  if len(words) == 4:
   w1[k-1][words[0]+words[1]+words[2]] = int(words[3]) 

  
for i in label:
 v += len(w[i])  
with open(finput4,'r') as f4:
 for line in f4:
  line=re.sub('[\(\)\{\}<>]', ' ', line)
  line= re.sub(r'[?|$|.|!|,|"|:|;|-|_|>|<|`|@|#|%|^|&|*|\|/|+|=|~|\']',r'',line)
  line=line.replace("-"," ")
  words = line.split()
  p_msg = {}
  for count,word in enumerate(words[1:]):
   for i in label:
    if count ==0:
     p_msg[i] = math.log(float((1+w[i][word])/(sum(w[i].values())+v)))
    elif count == 1:
     p_msg[i] += math.log(float((1+w1[i][words[count-1]+words[count]])/(w[i][words[count-1]]+v)))  
    else:
     p_msg[i] += math.log(float((1+w1[i][words[count-2]+words[count-1]+words[count]])/(w[i][words[count-2]+words[count-1]]+v)))   
  language = max(p_msg,key=p_msg.get)
  f5.write(str(label[language]))
  f5.write("\n")
  
 