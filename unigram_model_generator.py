#Unigrams Generator 
#Command Line Argument #1 - Input Pre-Processed File
#Command Line Argument #2 - Desired Output Unigram Model File Name
import sys
import nltk
from collections import defaultdict
#!/usr/bin/python
fin=str(sys.argv[1])
fout= str(sys.argv[2])
foutput=open(fout,'w',errors='ignore')
sentence_text=[]
with open(fin,'r',errors='ignore') as inp:
 for line in inp:
  if line=='\n':
   continue
  else:
   sentence_text.append(line.rstrip('\n'))

words_total=[]

for line in sentence_text:
 words=line.split()
 for w in words: 
  words_total.append(w)


words_u = list(set(words_total))
word_dict={}
for iter1 in words_u:
 count=0
 for iter2 in words_total:
  if iter1==iter2:
   count+=1
 word_dict[iter1]=count

for key1,value1 in word_dict.items() :
 print (key1,value1)
 foutput.write(str(key1) + " " + str(value1))
 foutput.write("\n")
