import sys
import math
from collections import defaultdict
import itertools
import codecs
import re
finput1 = str(sys.argv[1])
#finput2 = str(sys.argv[2])
finput3 = str(sys.argv[2])
search = '***NAMEDCLASSES***'
label = {}
j = 0
w = {}
label1 = {}
k = 0
w1 = {}
v = 0

with open(finput1,'r') as f1:
 for line in f1:
  if line.startswith(search):
   words = line.split()
   label[j] = words[1]
   w[j] = defaultdict(int)
   j = j+1
   line = next(f1)
  words = line.split()
  if len(words) == 2:
   w[j-1][words[0]] = int(words[1]) 
for i in label:
 v += len(w[i])  
with open(finput3,'r') as f3:
 for line in f3:
  line=re.sub('[\(\)\{\}<>]', ' ', line)
  line= re.sub(r'[?|$|.|!|,|"|:|;|-|_|>|<|`|@|#|%|^|&|*|\|/|+|=|~|\']',r'',line)
  line=line.replace("-"," ")
  words = line.split()
  p_msg = defaultdict(int)
  for count,word in enumerate(words):
   for i in label:
    p_msg[i] += math.log(float((1+w[i][word])/(sum(w[i].values())+v)))
  language = max(p_msg,key=p_msg.get)
  print(label[language]) 
  
 
