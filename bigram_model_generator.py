#First Command Line Argument  - Input Pre-Processed File 
#Second Command Line Argument - Desired Output File Name
import sys
import nltk
from collections import defaultdict
#!/usr/bin/python
fin=str(sys.argv[1])
fout= str(sys.argv[2])
foutput=open(fout,'w',errors='ignore')
sentence_text=[]
with open(fin,'r',errors='ignore') as inp:
 for line in inp:
  if line=='\n':
   continue
  else:
   sentence_text.append(line.rstrip('\n'))
bigrams_total=[] 
for line in sentence_text:
 words=line.split()
 for v, w in zip(words[:-1], words[1:]):
  temp=v+" "+w
  temp.rstrip("\n")
  bigrams_total.append(temp)
bigrams_u = list(set(bigrams_total))
bigrams_dict={}
for iter1 in bigrams_u:
 count=0
 for iter2 in bigrams_total:
  if iter1==iter2:
   count+=1
 bigrams_dict[iter1]=count
for key1,value1 in bigrams_dict.items() :
 foutput.write(str(key1) + " " + str(value1))
 foutput.write("\n")

